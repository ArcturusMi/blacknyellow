from django.db import models

# Create your models here.
class Agenda(models.Model):
    title = models.CharField(max_length=100)
    date = models.CharField(max_length=2)
    month = models.CharField(max_length=15)

    def __str__(self):
        return self.title