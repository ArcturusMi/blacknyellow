from django import forms
from . import models


class AddAgenda(forms.ModelForm):
    class Meta:
        model = models.Agenda
        fields = ['title','date','month']