from django.urls import path
from . import views
app_name = "web2"
urlpatterns = [
    path('', views.index, name="home"),
    path('experience/', views.experience, name="experience"),
    path('photos/', views.photos, name="photos"),
    path('reviews/', views.reviews, name="reviews"),
    path('bonus/', views.bonus, name="bonus"),
    path('agenda/', views.agenda, name="agenda"),
    path('forms/',views.form, name="forms"),
    path('delete/',views.delpos, name="deleteagenda"),
]
