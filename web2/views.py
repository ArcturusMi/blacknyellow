from django.shortcuts import render,redirect
from .models import Agenda
from . import forms

# Create your views here.
def index(request):
    title = "HOME"
    return render(request,'Home.html', {"title": title})

def experience(request):
    title = "EXPERIENCE"
    return render(request,'Experience.html', {"title": title})

def photos(request):
    title = "PHOTOS"
    return render(request, 'Photos.html', {"title": title})

def reviews(request):
    title = "REVIEWS"
    return render(request, 'Reviews.html', {"title": title})

def bonus(request):
    title = "BONUS"
    return render(request, 'Bonus.html', {"title": title})

def form(request):
    title = "FORMS"
    if request.method == 'POST':
        form = forms.AddAgenda(request.POST)
        if form.is_valid():
            form.save()
            form = forms.AddAgenda()

    else:
        form = forms.AddAgenda()
    return render(request, 'Form.html', {"title" :title,"form" : form})

def agenda(request):
    title = "AGENDA"
    agendas = Agenda.objects.all()
    return render(request, 'Agenda.html', {"title": title, "agendas":agendas})


def delpos(request):
    if(request.method=="POST"):
        agendas = Agenda.objects.all()
        for obj in agendas:
            if obj.id == int(request.POST['id']):
                obj.delete()
                return redirect('/agenda') 